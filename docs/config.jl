using BezierBernsteinMethods 

package_info = Dict(
    "modules" => [BezierBernsteinMethods],
    "authors" => "Rene Hiemstra and contributors",
    "name" => "BezierBernsteinMethods.jl",
    "repo" => "https://gitlab.com/feather-ecosystem/core/BezierBernsteinMethods",
    "pages" => [
        "About"  =>  "index.md"
        "API"  =>  "api.md"
    ],
)

DocMeta.setdocmeta!(BezierBernsteinMethods, :DocTestSetup, :(using BezierBernsteinMethods); recursive=true)

# if docs are built for deployment, fix the doctests
# which fail due to round off errors...
if haskey(ENV, "DOC_TEST_DEPLOY") && ENV["DOC_TEST_DEPLOY"] == "yes"
    doctest(BezierBernsteinMethods, fix=true)
end

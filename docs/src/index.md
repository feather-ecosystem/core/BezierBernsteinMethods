# BezierBernsteinMethods.jl
[![pipeline status](https://gitlab.com/feather-ecosystem/core/BezierBernsteinMethods/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/core/BezierBernsteinMethods/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/core/BezierBernsteinMethods/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/core/BezierBernsteinMethods/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/BezierBernsteinMethods/branch/master/graph/badge.svg?token=jCAgaonRgx)](https://codecov.io/gl/feather-ecosystem:core/BezierBernsteinMethods)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/BezierBernsteinMethods)

***

This package implements Bezier-Bernstein methods on simplicial meshes. Further reading:

1. Kirby, R.C. Fast simplicial finite element algorithms using Bernstein polynomials. Numer. Math. 117, 631–652 (2011). [https://doi.org/10.1007/s00211-010-0327-2](https://doi.org/10.1007/s00211-010-0327-2)
2. Mark Ainsworth, Gaelle Andriamaro, and Oleg Davydov. Bernstein-Bézier Finite Elements of Arbitrary Order and Optimal Assembly Procedures. SIAM J. Sci. Comput. 33, 6 (November 2011), 3087–3109. [https://doi.org/10.1137/11082539X](https://doi.org/10.1137/11082539X)
3. Kirby, R.C. Low-Complexity Finite Element Algorithms for the de Rham Complex on Simplices. SIAM Journal on Scientific Computing 36:2, A846-A868 (2014). [https://doi.org/10.1137/130927693](https://doi.org/10.1137/130927693)

!!! tip
    This package is a part of the [`Feather`](https://gitlab.com/feather-ecosystem) project. It is tightly integrated into the ecosystem of packages provided by Feather. If you are interested in applications of the functionality implemented in this package, please visit the main documentation of the [`ecosystem`](https://feather-ecosystem.gitlab.io/feather/)
# BezierBernsteinMethods.jl


[![pipeline status](https://gitlab.com/feather-ecosystem/core/BezierBernsteinMethods/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/core/BezierBernsteinMethods/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/core/BezierBernsteinMethods/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/core/BezierBernsteinMethods/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/BezierBernsteinMethods/branch/master/graph/badge.svg?token=jCAgaonRgx)](https://codecov.io/gl/feather-ecosystem:core/BezierBernsteinMethods)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/BezierBernsteinMethods)
